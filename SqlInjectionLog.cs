﻿using Serilog;
using Serilog.Events;
using Serilog.Sinks.SystemConsole.Themes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SqlInjectionDemo
{
    public class SqlInjectionLog
    {
        public static readonly ILogger Output;

        static SqlInjectionLog()
        {
            Output = new LoggerConfiguration()
                    .MinimumLevel.Verbose()
                    // Logging test output to console allows it to be captured in the report.
                    // Disable theme to prevent excess newlines
                    .WriteTo.Console(standardErrorFromLevel: LogEventLevel.Error, theme: ConsoleTheme.None)
                    .WriteTo.Trace()
                    .CreateLogger();
        }
    }
}
