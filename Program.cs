﻿using Dapper;
using Npgsql;
using PowerArgs;
using Serilog;
using Serilog.Events;
using Serilog.Sinks.SystemConsole.Themes;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SqlInjectionDemo
{
    [ArgExceptionBehavior(ArgExceptionPolicy.StandardExceptionHandling)]
    public class Program
    {
        static void Main(string[] args)
        {
            Args.InvokeAction<Program>(args);
            Console.ReadLine();
        }

        [ArgActionMethod]
        public void BasicInjection()
        {
            DatabaseManager.RecreateDb();
            BasicInjectionDemo.DemoSelectInjection();
        }

        [ArgActionMethod]
        public void BasicUpdateInjection()
        {
            DatabaseManager.RecreateDb();
            BasicInjectionDemo.DemoUpdateInjection();
        }

        [ArgActionMethod]
        public void BasicInjectionFixed()
        {
            DatabaseManager.RecreateDb();
            BasicInjectionDemo.DemoParameterized();
        }

        [ArgActionMethod]
        public void ComplexInjection()
        {
            DatabaseManager.RecreateDb();
            MoreComplexInjectionDemo.DemoUpdateInjection();
        }

        [ArgActionMethod]
        public void ComplexInjectionWhitelisted()
        {
            DatabaseManager.RecreateDb();
            MoreComplexInjectionDemo.DemoWhitelist();
        }
    }
}
