﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SqlInjectionDemo
{
    public static class BasicInjectionDemo
    {
        public static readonly string Injection = "0 OR true";

        public static void DemoSelectInjection()
        {
            using (var conn = DatabaseManager.MakeConnection())
            {
                // The tell is that it CONCATENATES THE STRING
                var result = conn.Query<Data>(
                    "SELECT * FROM data WHERE data_id = " + Injection.ToString()
                );

                // We were expecting one result, but we get the whole table.
                foreach (var r in result)
                {
                    SqlInjectionLog.Output.Information(r.ToString());
                }
            }
        }

        public static void DemoUpdateInjection()
        {
            using (var conn = DatabaseManager.MakeConnection())
            {
                string newData = "hahaha you were hacked";

                // The tell is that it CONCATENATES THE STRING
                conn.Execute(String.Format(
                    @"
                    UPDATE data
                    SET text_data = '{0}'
                    WHERE data_id = {1}
                    ",
                    newData,
                    Injection
                ));

                var result = conn.Query<Data>("SELECT * FROM data");

                // Every single row got updated!
                foreach (var r in result)
                {
                    SqlInjectionLog.Output.Information(r.ToString());
                }
            }
        }

        public static void DemoParameterized()
        {
            try
            {
                using (var conn = DatabaseManager.MakeConnection())
                {
                    // NO concatenation
                    var result = conn.Query<Data>(
                        "SELECT * FROM data WHERE data_id = @dataid",
                        param: new { dataid = Injection }
                    );

                    foreach (var r in result)
                    {
                        SqlInjectionLog.Output.Information(r.ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                SqlInjectionLog.Output.Error(ex, "Error executing SQL");
            }
        }
    }
}
