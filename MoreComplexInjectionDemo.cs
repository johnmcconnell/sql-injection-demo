﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SqlInjectionDemo
{
    public static class MoreComplexInjectionDemo
    {
        private static readonly Dictionary<string, object> InjectionValues = new Dictionary<string, object>()
        {
            { "int_data", 50 },
            { "text_data\" = 'hahaha you were hacked'; DELETE FROM data WHERE data_id != 10; UPDATE data SET \"text_data", "hahaha you were hacked" }
        };

        private static readonly HashSet<string> ValidColumnNames = new HashSet<string>(new string[] { nameof(Data.data_id), nameof(Data.text_data), nameof(Data.int_data) });


        public static void DemoUpdateInjection()
        {
            using (var conn = DatabaseManager.MakeConnection())
            {
                Dictionary<string, object> paramValues = new Dictionary<string, object>();


                string query = "UPDATE data SET " +
                    String.Join(
                        ", ",
                        InjectionValues.Select((p, i) =>
                        {
                            string paramName = "value" + i;

                            paramValues[paramName] = p.Value;

                            return String.Format(
                                // Quoting isn't enough!
                                "\"{0}\" = {1}",
                                p.Key,
                                "@" + paramName
                            );
                        })
                    )
                    + " WHERE data_id = @id"
                ;

                paramValues["id"] = 10;

                SqlInjectionLog.Output.Information(query);

                conn.Execute(query, param: paramValues);

                // The tell is that it CONCATENATES THE STRING
                var result = conn.Query<Data>("SELECT * FROM data");

                // We were expecting one result, but we get the whole table.
                foreach (var r in result)
                {
                    SqlInjectionLog.Output.Information(r.ToString());
                }
            }
        }

        public static void DemoWhitelist()
        {
            try
            {
                foreach (var k in InjectionValues.Keys)
                {
                    if (!ValidColumnNames.Contains(k))
                    {
                        throw new Exception("Invalid column name: " + k);
                    }
                }


                using (var conn = DatabaseManager.MakeConnection())
                {
                    Dictionary<string, object> paramValues = new Dictionary<string, object>();


                    string query = "UPDATE data SET " +
                        String.Join(
                            ", ",
                            InjectionValues.Select((p, i) =>
                            {
                                string paramName = "value" + i;

                                paramValues[paramName] = p.Value;

                                return String.Format(
                                    // Quoting isn't enough!
                                    "\"{0}\" = {1}",
                                    p.Key,
                                    "@" + paramName
                                );
                            })
                        )
                        + " WHERE data_id = @id"
                    ;

                    paramValues["id"] = 10;

                    conn.Execute(query, param: paramValues);

                    // The tell is that it CONCATENATES THE STRING
                    var result = conn.Query<Data>("SELECT * FROM data");

                    // We were expecting one result, but we get the whole table.
                    foreach (var r in result)
                    {
                        SqlInjectionLog.Output.Information(r.ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                SqlInjectionLog.Output.Error(ex, "");
            }
        }
    }
}
