﻿using Dapper;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SqlInjectionDemo
{
    public class Data
    {
        public int data_id { get; set; }
        public string text_data { get; set; }
        public int int_data { get; set; }

        public override string ToString()
        {
            return String.Format(
                "Data(data_id={0}, text_data={1}, int_data={2})",
                this.data_id,
                this.text_data,
                this.int_data
            );
        }
    }

    public class DatabaseManager
    {
        private const string ConnectionStringConfigName = "testdb";
        private const string WorkingDbName = "sqlinjection";
        private const string AdminDbName = "postgres";

        private static readonly Random RNG = new Random();


        private static string GetFullConnectionString(string configName, string dbName)
        {
            return ConfigurationManager.ConnectionStrings[configName].ConnectionString.TrimEnd(';') + ";Database=" + dbName;
        }

        public static IDbConnection MakeConnection()
        {
            return new NpgsqlConnection(GetFullConnectionString(ConnectionStringConfigName, WorkingDbName));
        }

        public static IDbConnection MakeAdminConnection()
        {
            return new NpgsqlConnection(GetFullConnectionString(ConnectionStringConfigName, AdminDbName));
        }

        public static void RecreateDb()
        {
            using (var conn = MakeAdminConnection())
            {
                SqlInjectionLog.Output.Information("Recreating DB");
                conn.Execute("DROP DATABASE IF EXISTS " + WorkingDbName);
                conn.Execute("CREATE DATABASE " + WorkingDbName);
            }

            using (var conn = MakeConnection())
            {
                SqlInjectionLog.Output.Information("Create data table");
                conn.Execute(@"
                    CREATE TABLE data (
                        data_id SERIAL PRIMARY KEY,
                        text_data TEXT NOT NULL,
                        int_data INTEGER NOT NULL
                    )
                ");


                SqlInjectionLog.Output.Information("Inserting data");
                foreach (int i in Enumerable.Range(0, 50))
                {
                    byte[] bytes = new byte[1024];
                    RNG.NextBytes(bytes);

                    conn.Execute(
                        "INSERT INTO data (text_data, int_data) VALUES (@tdata, @idata)",
                        param: new
                        {
                            tdata = Convert.ToBase64String(bytes),
                            idata = RNG.Next()
                        }
                    );
                }
            }
        }
    }
}
